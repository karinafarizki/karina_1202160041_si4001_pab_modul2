package com.example.karina_1202160041_si4001_pab_modul2;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.lang.annotation.Documented;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    String[] tujuan = {"Jakarta (Rp 85.000)", "Cirebon (Rp 150.000)", "Bekasi (Rp 70.000)"};
    int[] harga = {85000, 150000, 70000};
    Integer itemPrice;
    String selectedTujuan = "";
    TextView tanggal, tanggal2;
    TextView waktu, waktu2;
    EditText tiket;
    TextView txtDate, txtTime, txtDate2, txtTime2;
    Switch aSwitch;
    DatePickerDialog datePickerDialog;
    TimePickerDialog timePickerDialog;
    int year, month, day;
    int jam, menit;
    String amPm;
    private TextView nol, top_up;
    Calendar calendar;
    final Context context = this;
    boolean pp = false;
    Button beli;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        top_up = findViewById(R.id.top_up);
        nol = findViewById(R.id.nol);
        aSwitch = findViewById(R.id.pulang);
        tiket = findViewById(R.id.jumlah_tiket2);

        boolean isComplete = false;

        if (getIntent().hasExtra("berhasil")) {
            isComplete = getIntent().getBooleanExtra("berhasil",false);
            if (isComplete) {
                TextView yes = findViewById(R.id.yes);
                yes.setVisibility(View.VISIBLE);
            }
        }


        top_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater li = LayoutInflater.from(context);
                View prompts = li.inflate(R.layout.prompts, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

                alertDialogBuilder.setView(prompts);

                final EditText input = (EditText) prompts.findViewById(R.id.input);

                alertDialogBuilder.setCancelable(false).setPositiveButton("Tambah Saldo", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Integer currSaldo = Integer.parseInt(nol.getText().toString()) + Integer.parseInt(input.getText().toString());
                        nol.setText(currSaldo.toString());
                        Toast.makeText(MainActivity.this, "Top Up Berhasil", Toast.LENGTH_LONG).show();
                    }
                })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        Spinner spin = findViewById(R.id.tujuan2);
        spin.setOnItemSelectedListener(this);

        ArrayAdapter array = new ArrayAdapter(this, android.R.layout.simple_spinner_item, tujuan);
        array.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(array);

        tanggal = findViewById(R.id.tanggal);
        waktu = findViewById(R.id.waktu);

        tanggal2 = findViewById(R.id.tanggal2);
        waktu2 = findViewById(R.id.waktu2);


        txtDate = findViewById(R.id.date);
        txtDate2 = findViewById(R.id.date2);
        txtTime = findViewById(R.id.time);
        txtTime2 = findViewById(R.id.time2);

        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    tanggal2.setVisibility(View.VISIBLE);
                    waktu2.setVisibility(View.VISIBLE);
                    pp = true;
                } else {
                    tanggal2.setVisibility(View.GONE);
                    waktu2.setVisibility(View.GONE);
                    pp = false;
                }
            }
        });


        waktu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timePick(txtTime);
            }
        });
        waktu2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timePick(txtTime2);
            }
        });

        tanggal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePick(txtDate);
            }
        });

        tanggal2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePick(txtDate2);
            }
        });
    }

    private void datePick(final TextView datepick) {
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        datePickerDialog = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                datepick.setText(day + "-" + (month + 1) + "-" + year);
            }
        }, year, month, day);
        datePickerDialog.show();
    }

    private void timePick(final TextView timePick) {
        calendar = calendar.getInstance();
        jam = calendar.get(Calendar.HOUR_OF_DAY);
        menit = calendar.get(Calendar.MINUTE);

        timePickerDialog = new TimePickerDialog(MainActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                if (hour >= 12) {
                    amPm = " PM";
                } else {
                    amPm = " AM";
                }
                timePick.setText(String.format("%02d:%02d", hour, minute) + amPm);
            }
        }, jam, menit, false);
        timePickerDialog.show();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        itemPrice = harga[i];
        selectedTujuan = adapterView.getItemAtPosition(i).toString();

        Toast.makeText(getApplicationContext(), tujuan[i], Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    public void beli(View view) {

        if (tiket.getText().toString().isEmpty() || tiket.getText().toString().equals("0")) {
            Toast.makeText(MainActivity.this, "Isi Jumlah Tiket", Toast.LENGTH_LONG).show();
            return;
        }

        int jumlah = Integer.parseInt(tiket.getText().toString());
        int currSaldo = Integer.parseInt(nol.getText().toString());
        int hargaTotal = itemPrice * jumlah;

        if(pp)
            hargaTotal *= 2;

        if (currSaldo < hargaTotal) {
            Toast.makeText(MainActivity.this, "Saldo Tidak Mencukupi", Toast.LENGTH_LONG).show();
            return;

        }


        Intent intent = new Intent(MainActivity.this, Summary.class);
        intent.putExtra("tujuan", selectedTujuan);
        intent.putExtra("tgl_berangkat", txtDate.getText() + " - " + txtTime.getText());
        intent.putExtra("tgl_pulang", txtDate2.getText() + " - " + txtTime2.getText());
        intent.putExtra("jml_tiket", jumlah);
        intent.putExtra("harga_total", hargaTotal);
        startActivity(intent);

    }
}
