package com.example.karina_1202160041_si4001_pab_modul2;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        getSupportActionBar().hide();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent theIntent =  new Intent(SplashActivity.this, MainActivity.class);
                SplashActivity.this.startActivity(theIntent);
                SplashActivity.this.finish();
            }
        },1000);
    }
}
