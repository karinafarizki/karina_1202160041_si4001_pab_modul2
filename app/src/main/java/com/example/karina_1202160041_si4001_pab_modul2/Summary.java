package com.example.karina_1202160041_si4001_pab_modul2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Summary extends AppCompatActivity {

    TextView Tujuan, Tanggal, Pulang, Jumlah, nominal, yes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.summary);

        Tujuan = findViewById(R.id.textTujuan);
        Tanggal = findViewById(R.id.textTanggal);
        Pulang = findViewById(R.id.textPulang);
        Jumlah = findViewById(R.id.textJumlah);
        nominal = findViewById(R.id.nominal);

        Tujuan.setText(getIntent().getStringExtra("tujuan"));
        Tanggal.setText(getIntent().getStringExtra("tgl_berangkat"));
        Pulang.setText(getIntent().getStringExtra("tgl_pulang"));
        Jumlah.setText(Integer.toString(getIntent().getIntExtra("jml_tiket", 0)));
        nominal.setText(Integer.toString(getIntent().getIntExtra("harga_total", 0)));


    }

    public void konfirmasi(View view) {
        Intent intent = new Intent(Summary.this, MainActivity.class);
        intent.putExtra("berhasil", true);
        startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        finish();
    }
}
